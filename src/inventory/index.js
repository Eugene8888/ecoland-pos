import React,{Component} from 'react'
import {Link} from 'react-router-dom'
import BootstrapTable from 'react-bootstrap-table-next';
import Modal from '../shared/Modal'
import { FormGroup, Label, Input,Progress,ListGroupItem,ListGroup} from 'reactstrap';
import Dropzone from 'react-dropzone'
import classNames from 'classnames'
import axios from 'axios'
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import cellEditFactory from 'react-bootstrap-table2-editor';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import currencyFormatter from 'currency-formatter'
const { SearchBar } = Search;
const defaultItem={ name:"",
    category:"",
    pricePerCount: 0,
    pricePerPound: 0,
    pricePerKG:0}
export default class Employee extends Component{
    constructor(props) {
        super(props);
        this.state={
            open:false,
            inlineEdit:false,
            sidebar:false,
            openFileUpload:false,
            xlsx:null,
            selected:[],
            inventoryItems:[],
            uploadProgress:'0',
            customers:[],
            startUpload:true,
            item:{...defaultItem}
        }
    }
    openModal(){
        this.setState({open:true})
    }
    onClose(){
        this.setState({open:false, item:{...defaultItem}})
    }
    onInputChange(e){
        let {item} = this.state;
        item[e.target.name] = e.target.value
        this.setState({item})
    }
    handleFileSubmit(){
        if(!this.state.xlsx){
            alert('No file was selected. Please selected a file to upload.')

        }else {
            const size = this.state.xlsx.size;
            const config = {
                onUploadProgress: (progressEvent) => {
                    this.setState({uploadProgress:(progressEvent.loaded / size )*100 >100? 100 :((progressEvent.loaded / size )*100 >100).toFixed(0) })
                },
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
            let formData = new FormData();
            formData.append("xlsx", this.state.xlsx);
            axios.post('/api/v1/inventory/item/xlsx',formData,config).then((response)=>{
                let {inventoryItems} =this.state;
                this.setState({inventoryItems: inventoryItems.concat(response.data)})
                NotificationManager.success('Data has been imported from excel file to database',"Import Data Succeed");
                this.closeFileUpload()


            }).catch((err)=>{
                console.log(err)
            })
        }

    }
    async componentDidMount(){

        axios.get('/api/v1/inventory/item').then(({data})=>{
            this.setState({inventoryItems:data})
        }).catch((err)=>{
            console.log(err)
        })
    }
    closeFileUpload(){
        this.setState({uploadProgress:0,startUpload:false,xlsx:null,openFileUpload:false})
    }
    onDrop = (acceptedFiles, rejectedFiles) => {
        if(rejectedFiles.length >0){
            alert('Only accept .xlsx file, please check your file type')
        }else {
            if(acceptedFiles.length ===1 ){
                this.setState({xlsx:acceptedFiles[0]})
            }else {
                alert('Only accept one file');
            }
        }
    }



    enableInlineEdit(){
        if(!this.state.inlineEdit){
            NotificationManager.warning('Inline edit is enabled, please edit your data carefully','Inline Edit Enabled');
        }else {
            NotificationManager.success(null,'Inline Edit disabled');
        }
        this.setState({inlineEdit:!this.state.inlineEdit})
    }
    onSelect(row, isSelect){
        if (isSelect) {
            this.setState(() => ({
                selected: [...this.state.selected, row._id]
            }));
        } else {
            this.setState(() => ({
                selected: this.state.selected.filter(x => x !== row._id)
            }));
        }
    }
    handleOnSelectAll = (isSelect, rows) => {
        const ids = rows.map(r => r._id);
        if (isSelect) {
            this.setState(() => ({
                selected: ids
            }));
        } else {
            this.setState(() => ({
                selected: []
            }));
        }
    };
    deleteItem(){
        axios.delete('/api/v1/inventory/item',{data:{_ids:this.state.selected}}).then((response)=>{
            let {inventoryItems} = this.state;
            this.setState({inventoryItems:inventoryItems.filter((item)=>!this.state.selected.includes(item._id)),selected:[]})
        }).catch((err)=>{
            NotificationManager.error(err.message, 'Database Error');
        })
    }
    onItemSubmit(){
        axios.post('/api/v1/inventory/item',this.state.item).then(({data})=>{
            let {inventoryItems} = this.state;
            inventoryItems.unshift(data)
            this.setState({inventoryItems})
            this.onClose()
        }).catch((err)=>{

            NotificationManager.error(err.response.data.message, 'Database Error');
        })
    }
    render() {
        const {open,item,openFileUpload,uploadProgress,inventoryItems,inlineEdit,selected} = this.state;
        const selectRow = {
            mode: 'checkbox',
            selected: selected,
            onSelect:this.onSelect.bind(this),
            onSelectAll: this.handleOnSelectAll.bind(this)
        };
        const cellEdit = cellEditFactory({
            mode: 'click',
            beforeSaveCell(oldValue, newValue, row, column,done){
                setTimeout(() => {
                    row[column.dataField]= newValue;
                    axios.put('/api/v1/inventory/item',row).then(({data})=>{

                        NotificationManager.success('You edited '+data.name, 'Data Saved');
                        done()

                    }).catch((err)=>{
                        NotificationManager.error(err.response.data.message, 'Database Error');
                    })
                },0);
                return { async: true };

            },
            nonEditableRows:()=>{
                if(!inlineEdit){
                    return inventoryItems.map((item)=>{
                        return item._id
                    })
                }
                else {
                    return []
                }
            }
        });
        const columns = [{
                dataField: 'name',
                text: 'Name',
                headerAlign: 'center',
                align:'center'
            },{
                dataField: 'category',
                text: 'Category',
                headerAlign: 'center',
                headerClasses:'hideCol',
                classes:'hideCol',
                align:'center'
            }, {
                dataField: 'pricePerCount',
                text: 'Per Count',
                headerAlign: 'center',
                align:'center',
                formatter:(cell,row)=>priceFormatter(cell,"USD")
            },
                {
                    dataField: 'pricePerPound',
                    text: 'Per Pound',
                    headerAlign: 'center',
                    align:'center',
                    formatter:(cell,row)=>priceFormatter(cell,"USD")
                },
                {
                    dataField: 'pricePerKG',
                    text: 'Per KG',
                    headerAlign: 'center',
                    align:'center',
                    formatter:(cell,row)=>priceFormatter(cell,"USD")
                }
                ]

        return (
                <div>
                    <ToolkitProvider
                        keyField="_id"
                        data={ inventoryItems }
                        columns={ columns }
                        search

                    >
                        {
                            (props) => {

                                return  <div>
                                    <div
                                        className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 border-bottom shadow-sm header fixed-top">
                                        <Link  style={{textDecoration:'none'}} className=" textColor my-0 mr-md-auto font-weight-normal" to={'/'}><h5 >Inventory</h5></Link>
                                        <div><SearchBar { ...props.searchProps } /></div>
                                        <nav className="my-2 my-md-0 mr-md-3">
                                            <a className="p-2 text-dark" href="#" onClick={this.openModal.bind(this)} >Add New</a>
                                            <a className="p-2 text-dark" href="#" onClick={()=>{this.setState({openFileUpload:true})}} >Upload Excel File</a>
                                            <a className="p-2 " href="#" style={{color:inlineEdit?'red':"#343a40"}} onClick={this.enableInlineEdit.bind(this)}>{inlineEdit?"Disable inline Edit":"Enable inline Edit"}</a>
                                            {selected.length >0 ?<a className="p-2 text-dark" href="#" disabled={true} onClick={this.deleteItem.bind(this)}>Delete({selected.length})</a> :null}
                                        </nav>
                                    </div>
                                    <div className={'appContainer'}>




                                        <BootstrapTable
                                            selectRow={selectRow}
                                            cellEdit={cellEdit}
                                            { ...props.baseProps }
                                        />


                                    </div>
                                </div>
                            }}
                    </ToolkitProvider>


                    <Modal title={'New Item'} open={open} onClose={this.onClose.bind(this)} onSubmit={this.onItemSubmit.bind(this)}>


                                <FormGroup>
                                    <Label >Name</Label>
                                    <Input type="text" value={item.name} onChange={this.onInputChange.bind(this)} name="name"  placeholder="name" />
                                </FormGroup>

                                <FormGroup>
                                    <Label >Category</Label>
                                    <Input type="text" name="category"  value={item.category} onChange={this.onInputChange.bind(this)} placeholder="category" />
                                </FormGroup>



                        <FormGroup>
                            <Label >Price per count</Label>
                            <Input type="text" name="pricePerCount" value={item.pricePerCount} onChange={this.onInputChange.bind(this)} placeholder="pricePerCount" />
                        </FormGroup>
                        <FormGroup>
                            <Label >Price per pound</Label>
                            <Input type="text" name="pricePerPound" value={item.pricePerPound} onChange={this.onInputChange.bind(this)}  placeholder="pricePerPound" />
                        </FormGroup>
                        <FormGroup>
                            <Label >Price per kg</Label>
                            <Input type="text" name="pricePerKG" value={item.pricePerKG} onChange={this.onInputChange.bind(this)}  placeholder="pricePerKG" />
                        </FormGroup>
                    </Modal>
                    <Modal title={'Insert New Inventory Item From Excel File'} open={openFileUpload} onClose={this.closeFileUpload.bind(this)} onSubmit={this.handleFileSubmit.bind(this)}>

                        <Dropzone onDrop={this.onDrop.bind(this)} accept={'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'}>
                            {({getRootProps, getInputProps, isDragActive}) => {
                                return (
                                    <div
                                        {...getRootProps()}
                                        className={classNames('dropzone', {'dropzone--isActive': isDragActive})}
                                    >
                                        <input {...getInputProps()} />
                                        {
                                            this.state.xlsx?<div className={'text-center'}>
                                                <i className={'fas fa-file fa-3x'}></i>
                                                <div> {this.state.xlsx.name}</div>
                                                {this.state.startUpload?<div><Progress value={uploadProgress} /><div className="text-center">{uploadProgress} %</div></div>:null}
                                            </div> : isDragActive ?
                                                <p>Drop files here...</p> :
                                                <p>Try dropping some files here, or click to select files to upload.</p>
                                        }
                                    </div>
                                )
                            }}
                        </Dropzone>
                    </Modal>
                    <NotificationContainer className={'notification'}/>
                </div>
        );
    }

}
function priceFormatter(data,code){
    return currencyFormatter.format(data, { code });
}
