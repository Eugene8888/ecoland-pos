import React, { Component } from 'react';
import './App.css';
import './bootstrap.min.css'
import Home from './home'
import Customer from './customer'
import Inventory from './inventory'
import Setting from './setting'
import Report from './report'
import Coupon from './coupon'
import { BrowserRouter as Router,Switch, Route } from "react-router-dom";
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import  Layout from './Layout'
import 'react-notifications/lib/notifications.css';
class App extends Component {
  render() {
    return (
        <Router>
            <Switch>
                <Route exact path={'/'} component={Home}/>
                <Layout>
                <Route exact path={'/customer'} component={Customer}/>
                <Route exact path={'/inventory'} component={Inventory}/>
                <Route exact path={'/coupon'} component={Coupon}/>
                <Route  path={'/setting'} component={Setting}/>
                <Route exact path={'/report'} component={Report}/>
                </Layout>
            </Switch>
        </Router>
    );
  }
}

export default App;
