import React,{Component} from 'react'
import {Link} from 'react-router-dom'
import BootstrapTable from 'react-bootstrap-table-next';
import Modal from '../shared/Modal'
import { FormGroup, Label, Input,Progress,ListGroupItem,ListGroup} from 'reactstrap';
import Dropzone from 'react-dropzone'
import classNames from 'classnames'
import axios from 'axios'
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import cellEditFactory from 'react-bootstrap-table2-editor';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import Sidebar from "react-sidebar";
const { SearchBar } = Search;
const data=[
            {dataField:'phoneNum',text:'Phone Number'},
            {dataField:'email',text:'Email'},
            {dataField:'street',text:'Street'},
            {dataField:'city',text:'City'},
            {dataField:'zipCode',text:'Zip Code'},
            {dataField:'country',text:'Country'},

            ]
const defaultCustomer={
    lastName:"",
    firstName: '',
    companyName: '',
    street:'',
    city:'',
    zipCode: '',
    state:'',
    country:''
};

export default class Employee extends Component{
    constructor(props) {
        super(props);
        this.state={
            open:false,
            inlineEdit:false,
            sidebar:false,
            openFileUpload:false,
            xlsx:null,
            columns:[{
                dataField: 'firstName',
                text: 'First Name',
                headerAlign: 'center',
                align:'center'
            },{
                dataField: 'lastName',
                text: 'Last Name',
                headerAlign: 'center',
                align:'center'
            }, {
                dataField: 'companyName',
                text: 'Company',
                headerAlign: 'center',
                align:'center'
            }
            ],
            selected:[],
            uploadProgress:'0',
            customers:[],
            startUpload:true,
            newCustomer:{
                ...defaultCustomer
            }
        }
    }
    openModal(){
        this.setState({open:true})
    }
    onClose(){
        this.setState({open:false, newCustomer:{
                ...defaultCustomer
            }})
    }
    onInputChange(e){
        let {newCustomer} = this.state;
        newCustomer[e.target.name] = e.target.value
        this.setState({newCustomer})
    }
    handleFileSubmit(){
        if(!this.state.xlsx){
        alert('No file was selected. Please selected a file to upload.')

        }else {
            const size = this.state.xlsx.size;
            const config = {
                onUploadProgress: (progressEvent) => {
                    this.setState({uploadProgress:(progressEvent.loaded / size )*100 >100? 100 :((progressEvent.loaded / size )*100 >100).toFixed(0) })
                },
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
            let formData = new FormData();
            formData.append("xlsx", this.state.xlsx);
            axios.post('/api/v1/customer/xlsx',formData,config).then((response)=>{
               let {customers} =this.state;
                this.setState({customers: customers.concat(response.data)})
                NotificationManager.success('Data has been imported from excel file to database',"Import Data Succeed");
                this.closeFileUpload()


            }).catch((err)=>{
                console.log(err)
            })
        }

    }
    async componentDidMount(){

        axios.get('/api/v1/customer').then(({data})=>{
            console.log(data)
            this.setState({customers:data})
        }).catch((err)=>{
            console.log(err)
        })
        let filtered = JSON.parse(await localStorage.getItem('columns'));
        let columns = this.state.columns;
        if(filtered){
            this.setState({columns:columns.concat(filtered)})
        }

    }
    closeFileUpload(){
        this.setState({uploadProgress:0,startUpload:false,xlsx:null,openFileUpload:false})
    }
    onDrop = (acceptedFiles, rejectedFiles) => {
        if(rejectedFiles.length >0){
            alert('Only accept .xlsx file, please check your file type')
        }else {
            if(acceptedFiles.length ===1 ){
                this.setState({xlsx:acceptedFiles[0]})
            }else {
                alert('Only accept one file');
            }
        }
    }
    saveCell(oldValue, newValue, row, column ,done){
       row[column.dataField]= newValue;
        axios.put('/api/v1/customer',row).then(({data})=>{

            NotificationManager.success('You edited '+data.firstName +' '+ data.lastName, 'Data Saved');

            done()
        }).catch((err)=>{
            console.log(err)
        })
    }
    enableInlineEdit(){
        if(!this.state.inlineEdit){
            NotificationManager.warning('Inline edit is enabled, please edit your data carefully','Inline Edit Enabled');
        }else {
            NotificationManager.success(null,'Inline Edit disabled');
        }
        this.setState({inlineEdit:!this.state.inlineEdit})
    }
    reSetFilter(){
        localStorage.removeItem('columns')
        this.setState({columns:[{
                dataField: 'firstName',
                text: 'First Name',
                headerAlign: 'center',
                align:'center'
            },{
                dataField: 'lastName',
                text: 'Last Name',
                headerAlign: 'center',
                align:'center'
            }, {
                dataField: 'companyName',
                text: 'Company',
                headerAlign: 'center',
                align:'center'
            }
            ],sidebar:false})
    }
   async onCheckBoxClick(item,e){
        console.log(e.target.checked)
        let defaultCol = this.state.columns
        if(e.target.checked){
            let columns =[];
            if(await localStorage.getItem('columns')){
                columns = JSON.parse(await localStorage.getItem('columns'))
            }
            console.log('col',columns)
            columns.push({...item,
                headerAlign: 'center',
                headerClasses:'hideCol',
                classes:'hideCol',
                align:'center'});
            localStorage.setItem('columns',JSON.stringify(columns));
            this.setState({columns:defaultCol.concat({...item,
                    headerAlign: 'center',
                    headerClasses:'hideCol',
                    classes:'hideCol',
                    align:'center'})})
        }else {
            let columns = JSON.parse(await localStorage.getItem('columns'));
            let neColumns = columns.filter((c)=>{
                return c.dataField !==item.dataField
            });
            localStorage.setItem('col',JSON.stringify(neColumns));
            this.setState({columns:defaultCol.filter((c)=>{
                        return c.dataField !== item.dataField
                })})
        }



    }
    onSelect(row, isSelect){
        if (isSelect) {
            this.setState(() => ({
                selected: [...this.state.selected, row._id]
            }));
        } else {
            this.setState(() => ({
                selected: this.state.selected.filter(x => x !== row._id)
            }));
        }
    }
    handleOnSelectAll = (isSelect, rows) => {
        const ids = rows.map(r => r._id);
        if (isSelect) {
            this.setState(() => ({
                selected: ids
            }));
        } else {
            this.setState(() => ({
                selected: []
            }));
        }
    };
    deleteCustomer(){
        axios.delete('/api/v1/customer',{data:{_ids:this.state.selected}}).then((response)=>{
            let {customers} = this.state;
            this.setState({customers:customers.filter((item)=>!this.state.selected.includes(item._id)),selected:[]})
        }).catch((err)=>{
            console.log(err)
        })
    }
    onSubmit(){
        axios.post('/api/v1/customer',this.state.newCustomer).then(({data})=>{
            let {customers} = this.state;
            customers.unshift(data)
            this.setState({customers})
            this.onClose()
        })
    }
    render() {
        const {open,newCustomer,openFileUpload,uploadProgress,customers,inlineEdit,columns,selected} = this.state;
        const selectRow = {
            mode: 'checkbox',
            selected: selected,
            onSelect:this.onSelect.bind(this),
            onSelectAll: this.handleOnSelectAll.bind(this)
        };
        const cellEdit = cellEditFactory({
            mode: 'click',
            beforeSaveCell: this.saveCell.bind(this),
            nonEditableRows:()=>{
               if(!inlineEdit){
                   return customers.map((item)=>{
                       return item._id
                   })
               }
               else {
                   return []
               }
            }
        });

        return (
            <Sidebar
                sidebar={
                    <ListGroup style={{width:200}} className={'filterContainer'}>
                        <ListGroupItem className='filterItem'>
                            <a href={'#'} onClick={this.reSetFilter.bind(this)}>Reset</a>
                            <a href={'#'} onClick={()=>{this.setState({sidebar:false})}}>Close</a></ListGroupItem>
                        <ListGroupItem> <b>Show Column</b></ListGroupItem>
                        {data.map((item,i)=>{
                            return <ListGroupItem key={'filter'+i} className='filterItem'>
                                <div>{item.text}</div><div><Input type="checkbox" checked={columns.find((c)=>{
                                    return c.dataField === item.dataField
                            })} onClick={this.onCheckBoxClick.bind(this,item)} /></div>
                            </ListGroupItem>
                        })}
                    </ListGroup>
                }
                open={this.state.sidebar}
                pullRight={true}
                styles={{ sidebar: { background: "white" } }}
            >
            <div>
                <ToolkitProvider
                    keyField="_id"
                    data={ customers }
                    columns={ columns }
                    search

                >
                    {
                        (props) => {

                   return  <div>
            <div
                className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 border-bottom shadow-sm header fixed-top">
                <Link  style={{textDecoration:'none'}} className=" textColor my-0 mr-md-auto font-weight-normal" to={'/'}><h5 >Customer</h5></Link>
                <div><SearchBar { ...props.searchProps } /></div>
                <nav className="my-2 my-md-0 mr-md-3">
                    <a className="p-2 text-dark" href="#" onClick={this.openModal.bind(this)} >Add New</a>
                    <a className="p-2 text-dark" href="#" onClick={()=>{this.setState({openFileUpload:true})}} >Upload Excel File</a>
                    <a className="p-2 " href="#" style={{color:inlineEdit?'red':"#343a40"}} onClick={this.enableInlineEdit.bind(this)}>{inlineEdit?"Disable inline Edit":"Enable inline Edit"}</a>
                    <a className="p-2 text-dark" href="#" onClick={()=>{this.setState({sidebar:true})}}>Columns</a>
                    {selected.length >0 ?<a className="p-2 text-dark" href="#" disabled={true} onClick={this.deleteCustomer.bind(this)}>Delete({selected.length})</a> :null}
                </nav>
            </div>
                <div className={'appContainer'}>




                                    <BootstrapTable
                                        selectRow={selectRow}
                                        cellEdit={cellEdit}
                                        { ...props.baseProps }
                                    />


                </div>
                    </div>
                        }}
                    </ToolkitProvider>


                <Modal title={'New Customer'} open={open} onClose={this.onClose.bind(this)}>

                    <div className={'row'}>
                        <div className={'col-lg-6'}>
                            <FormGroup>
                                <Label >First Name</Label>
                                <Input type="text" value={newCustomer.firstName} onChange={this.onInputChange.bind(this)} name="firstName"  placeholder="First Name" />
                            </FormGroup>
                        </div>
                        <div className={'col-lg-6'}>
                            <FormGroup>
                                <Label >Last Name</Label>
                                <Input type="text" name="lastName"  value={newCustomer.lastName} onChange={this.onInputChange.bind(this)} placeholder="Last Name" />
                            </FormGroup>
                        </div>
                    </div>



                    <FormGroup>
                        <Label >Company Name</Label>
                        <Input type="text" name="companyName" value={newCustomer.companyName} onChange={this.onInputChange.bind(this)} placeholder="Company Name" />
                    </FormGroup>
                    <FormGroup>
                        <Label >Street</Label>
                        <Input type="text" name="street" value={newCustomer.street} onChange={this.onInputChange.bind(this)}  placeholder="Street" />
                    </FormGroup>
                    <div className={'row'}>
                        <div className={'col-lg-6'}>
                            <FormGroup>
                                <Label >City</Label>
                                <Input type="text" name="city" value={newCustomer.city} onChange={this.onInputChange.bind(this)} placeholder="City" />
                            </FormGroup>
                        </div>
                        <div className={'col-lg-6'}>
                             <FormGroup>
                                <Label >Zip Code</Label>
                                <Input type="text" name="zipCode" value={newCustomer.zipCode} onChange={this.onInputChange.bind(this)} placeholder="Zip Code" />
                            </FormGroup>
                        </div>
                    </div>

                    <div className={'row'}>
                        <div className={'col-lg-6'}>
                            <FormGroup>
                                <Label >State</Label>
                                <Input type="text" name="state" value={newCustomer.state} onChange={this.onInputChange.bind(this)} placeholder="State" />
                            </FormGroup>
                        </div>
                        <div className={'col-lg-6'}>
                            <FormGroup>
                                <Label >Country</Label>
                                <Input type="text" name="country" value={newCustomer.country} onChange={this.onInputChange.bind(this)} placeholder="Country" />
                            </FormGroup>
                        </div>
                    </div>
                </Modal>
                <Modal title={'Insert New Customer From Excel File'} open={openFileUpload} onClose={this.closeFileUpload.bind(this)} onSubmit={this.handleFileSubmit.bind(this)}>

                    <Dropzone onDrop={this.onDrop.bind(this)} accept={'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'}>
                        {({getRootProps, getInputProps, isDragActive}) => {
                            return (
                                <div
                                    {...getRootProps()}
                                    className={classNames('dropzone', {'dropzone--isActive': isDragActive})}
                                >
                                    <input {...getInputProps()} />
                                    {
                                          this.state.xlsx?<div className={'text-center'}>
                                            <i className={'fas fa-file fa-3x'}></i>
                                            <div> {this.state.xlsx.name}</div>
                                              {this.state.startUpload?<div><Progress value={uploadProgress} /><div className="text-center">{uploadProgress} %</div></div>:null}
                                        </div> : isDragActive ?
                                            <p>Drop files here...</p> :
                                            <p>Try dropping some files here, or click to select files to upload.</p>
                                    }
                                </div>
                            )
                        }}
                    </Dropzone>
                </Modal>
                <NotificationContainer className={'notification'}/>
            </div>
            </Sidebar>
        );
    }

}
