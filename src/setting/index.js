import React,{Component} from 'react'
import Sidebar from "react-sidebar";
import {Link} from 'react-router-dom'
import {Input, ListGroup, ListGroupItem} from "reactstrap";
import Item from './Item'
const links=[
    {name:"Company Info",path:'/info'},
    {name:"About",path:'/about'}
]
export default class Employee extends Component{
    constructor(props) {
        super(props);
        this.state={

        }
    }

    render() {
        return (
            <Sidebar
                sidebar={
                    <ListGroup style={{width:200}}>
                        <ListGroupItem> <b>Setting</b></ListGroupItem>
                        {links.map((item,i)=>{
                            return <Link key={i} to={"/setting"+item.path}><ListGroupItem> {item.name}</ListGroupItem></Link>
                        })}
                    </ListGroup>
                }
                docked={true}
                styles={{ sidebar: { background: "white" } }}
            >
                <div className={'row'} style={{margin:50}}>
                    <div className={'col-lg-6'}>
                    <Item name={'Company Name'} value={'AE'}/>
                </div>
                </div>
            </Sidebar>
        );
    }

}