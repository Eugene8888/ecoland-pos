import React,{Component} from 'react'

export default class Item extends Component{
    constructor(props) {
        super(props);
        this.state={

        }
    }
    render(){

        return(
            <div style={{display:'flex',flexDirection:'row',justifyContent:'space-between',alignItem:'center'}}>
                <div>{this.props.name}</div>
                <div>{this.props.value}</div>
            </div>
        )
    }

}