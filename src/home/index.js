import React,{Component} from "react";
import AnimatedTitle from '../shared/AnimatedTitle'
export default class Home extends Component{
    constructor(props) {
        super(props);

    }
    navigate(key){
        this.props.history.push('/'+key)
    }
    render() {
        return (
            <div className="App">
                <header className="App-header">

                    <div style={{width:'100%'}} className={'row'}>
                        <div className={'col-lg-3 col-md-6 col-sm-6 Home-IconBtn'}
                             >
                            <i className="fas fa-users fa-5x"
                               onClick={this.navigate.bind(this,'customer')}/>
                            <div className={'text-center'}>Customer</div>
                        </div>
                        <div className={'col-lg-3 col-md-6 col-sm-6 Home-IconBtn'}

                        >
                            <i className="fas fa-clipboard-list fa-5x" onClick={this.navigate.bind(this,'inventory')}/>
                            <div className={'text-center'}>Inventory</div>
                        </div>
                        <div className={'col-lg-3 col-md-6 col-sm-6 Home-IconBtn'}
                            >
                            <i className="fas fa-file-invoice-dollar fa-5x"  onClick={this.navigate.bind(this,'coupon')} />
                            <div className={'text-center'}>Coupon</div>
                        </div>
                        <div className={'col-lg-3 col-md-6 col-sm-6 Home-IconBtn'}
                        >
                            <i className="fas fa-file-medical-alt fa-5x" onClick={this.navigate.bind(this,'setting')} />
                            <div className={'text-center'}>Report</div>
                        </div>

                    </div>
                    <div style={{width:'100%'}} className={'row'}>
                        <div className={'col-lg-3 col-md-6 col-sm-12 Home-IconBtn'}
                        >
                            <i className="fas fa-cash-register fa-5x" onClick={this.navigate.bind(this,'store')} />
                            <div className={'text-center'}>Store</div>
                        </div>
                        <div className={'col-lg-3 col-md-6 col-sm-12 Home-IconBtn'}
                        >
                            <i className="fas fa-cog fa-5x" onClick={this.navigate.bind(this,'setting')} />
                            <div className={'text-center'}>Setting</div>
                        </div>


                    </div>
                    <div style={{position:'fixed',bottom:0,paddingBottom:10,backgroundColor:'#282c34'}}>
                        <AnimatedTitle title={'Ecoland POS'} subTitle={'A POS designed for recycling industry'}/>
                    </div>
                </header>
            </div>
        );
    }

}
