import React,{Component} from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
export default class ModalCom extends Component{
    constructor(props) {
        super(props);
        this.state={

        }
    }
  submit(){
        this.props.onSubmit()
  }
    render(){
        const {title,open} = this.props;
        return (
            <Modal isOpen={open} >
                <ModalHeader toggle={this.toggle}>{title}</ModalHeader>
                <ModalBody>
                    {this.props.children}
                </ModalBody>
                <ModalFooter>
                    <Button style={{backgroundColor:'#282c34'}} onClick={this.submit.bind(this)}>Submit</Button>{' '}
                    <Button color="secondary" onClick={()=>{this.props.onClose()}}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }

}