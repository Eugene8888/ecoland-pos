const express = require( 'express');
const router = express.Router();
const customer = require('./routes/customer');
const item = require('./routes/item')
const auth = require('./routes/auth')
customer(router)
item(router)
auth(router)
module.exports = router;
