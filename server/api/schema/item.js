const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ids = require('short-id');
const itemSchema = new Schema({
    belongTo:{
        companyId:String,
        storeId:String,
    },
    id:{type:String,required:true,unique:true,default:ids.generate},
    name:{type:String,required:true},
    category:{type:String,required:true},
    pricePerCount:Number,
    pricePerPound:Number,
    pricePerKG:Number,
});

itemSchema.index({ "belongTo.companyId": 1, "belongTo.storeId": 1 ,id:1});
module.exports = mongoose.model('inventory',itemSchema ,'inventory');;