const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const customerSchema = new Schema({
    belongTo:{
        companyId:String,
        storeId:String,
    },
     firstName:{type:String,required:true},
     lastName:{type:String,required:true},
     companyName:String,
     phoneNum:String,
     email:String,
     street:String,
     city:String,
     state:String,
     zipCode:String,
     country:String
});
customerSchema.index({ "belongTo.companyId": 1, "belongTo.storeId": 1 });
module.exports = mongoose.model('customer',customerSchema ,'customer');;