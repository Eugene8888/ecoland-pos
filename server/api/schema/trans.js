const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transSchema = new Schema({
    id:{type:String,required:true},
    total:Number,
    subTotal:String,
    item:[]
});

module.exports = transSchema;