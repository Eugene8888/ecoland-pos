const CustomerModal  = require('../schema/customer')
const multer  = require('multer')
const upload = multer()
const  xlsxHelper = require('../util/xlsxHelper')
const XLSX = require('xlsx');
const async = require('async')
const route = function(router) {
    router.get('/customer',(req,res)=>{
        let query = req.query || {}
        CustomerModal.find(query,(err,doc)=>{
            if(!err){
                res.send(doc)
            }else {
                res.status(500).send(err)
            }

        })
    })
    router.post('/customer/xlsx',upload.single('xlsx'),(req,res)=>{
    xlsxHelper.toJson(XLSX.read(req.file.buffer, {type:'buffer'}),(data)=>{
        let returnData=[]

        async.each(data, function(item, callback) {

            // Perform operation on file here.
            console.log('Processing xlsx ');
            item.belongTo={
                companyId:req.user.companyId,
                storeId: req.user.storeId
            }
            insetNewCustomer(item,(err,doc)=>{
                returnData.push(doc)
                callback(err)
            })
        }, function(err) {
            // if any of the file processing produced an error, err would equal that error
            if( err ) {
                res.status(500).send(err)
            } else {
                res.send(returnData)
            }
        });
    })
    })
    router.put('/customer',(req,res)=>{


            CustomerModal.findOneAndUpdate({_id:req.body._id, "belongTo.companyId":req.user.companyId},req.body,{new:true},(err,doc)=>{
                if( err ) {
                    res.status(500).send(err)
                } else {
                    res.send(doc)
                }
            })

    })
    router.post('/customer',(req,res)=>{
        req.body.belongTo={
            companyId:req.user.companyId,
            storeId: req.user.storeId
        }
        insetNewCustomer(req.body,(err,doc)=>{
            if(!err){
                res.send(doc)
            }else {
                res.status(500).send(err)
            }
        })

    })
    router.delete('/customer',(req,res)=>{
        CustomerModal.deleteMany({ "belongTo.companyId":req.user.companyId, _id: { $in: req.body._ids}}, function(err) {
            if(!err){
                res.end()
            }else {
                console.log(err)
                res.status(500).send(err)
            }
        })
    })
}
function insetNewCustomer(data,cb){
    let customer = new CustomerModal(data)
    customer.save(cb)
}
module.exports = route;