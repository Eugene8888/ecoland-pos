const ItemModal  = require('../schema/item')
const multer  = require('multer')
const upload = multer()
const  xlsxHelper = require('../util/xlsxHelper')
const XLSX = require('xlsx');
const async = require('async');
const validator = require('validator');
const route = function(router) {
    router.get('/inventory/item',(req,res)=>{
        let query = req.query || {}
        ItemModal.find(query,(err,doc)=>{
            if(!err){
                res.send(doc)
            }else {
                res.status(500).send(err)
            }

        })
    })
    router.post('/inventory/item/xlsx',upload.single('xlsx'),(req,res)=>{
    xlsxHelper.toJson(XLSX.read(req.file.buffer, {type:'buffer'}),(data)=>{
        let returnData=[]
        async.each(data, function(item, callback) {

            // Perform operation on file here.
            console.log('Processing xlsx ');
            item.belongTo={
                companyId:req.user.companyId,
                storeId: req.user.storeId
            }
            insetNewInventoryItem(item,(err,doc)=>{
                returnData.push(doc)
                callback(err)
            })
        }, function(err) {
            // if any of the file processing produced an error, err would equal that error
            if( err ) {
                console.log(err)
                res.status(500).send(err)
            } else {
                res.send(returnData)
            }
        });
    })
    })
    router.put('/inventory/item',(req,res)=>{

            isValid(req.body,(valid,message)=>{
                if(valid){
                    ItemModal.findOneAndUpdate({_id:req.body._id, "belongTo.companyId":req.user.companyId},req.body,{new:true},(err,doc)=>{
                        if( err ) {
                            console.log(err)
                            res.status(500).send(err)
                        } else {
                            res.send(doc)
                        }
                    })
                }else {
                    res.status(500).send({message})
                }
            })


    })
    router.post('/inventory/item',(req,res)=>{
        req.body.belongTo={
            companyId:req.user.companyId,
            storeId: req.user.storeId
        }
            isValid(req.body,(valid,message)=>{
                if(valid){
                    insetNewInventoryItem(req.body,(err,doc)=>{
                        if(!err){
                            res.send(doc)
                        }else {
                            res.status(500).send(err)
                        }
                    })

                }else {
                    res.status(500).send({message})
                }
            })



    })
    router.delete('/inventory/item',(req,res)=>{
        ItemModal.deleteMany({ "belongTo.companyId":req.user.companyId, _id: { $in: req.body._ids}}, function(err) {
            if(!err){
                res.end()
            }else {
                console.log(err)
                res.status(500).send(err)
            }
        })
    })
}

function isValid(data,cb){
    let message=null;
    let valid = true;
    if(data.pricePerCount){
        if(!validator.isFloat(data.pricePerCount+"")){

            valid =false;
            message="Price per Count must be a number. Please check you input"
            cb(valid,message)
        }

    }
    if(data.pricePerPound){
        if(!validator.isFloat(data.pricePerPound+"")){

            valid =false;
            message="Price per Pound must be a number. Please check you input"
            cb(valid,message)
        }
    }
    if(data.pricePerKG){
        if(!validator.isFloat(data.pricePerKG+"")){

            valid =false;
            message="Price per KG must be a number. Please check you input"
            cb(valid,message)
        }
    }

        if(validator.isEmpty(data.name)){

            valid =false;
            message="Item name can not be empty"
            cb(valid,message)
        }
        console.log(valid)
    if(validator.isEmpty(data.category)){

        valid =false;
        message="Category name can not be empty"
        cb(valid,message)
    }
    cb(valid,message)
}
function insetNewInventoryItem(data,cb){
    let inventoryItem = new ItemModal(data)
    inventoryItem.save(cb)
}
module.exports = route;