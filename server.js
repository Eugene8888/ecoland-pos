const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const app = express();
const mongoose = require('mongoose');
mongoose.connect('mongodb://admin:power-pos123@ds056698.mlab.com:56698/power-pos',{ useNewUrlParser: true });
const  db = mongoose.connection;
const api  =require('./server/api/index');
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("connected to db!")
});
const port = process.env.PORT  || 8080;
const ip   = process.env.IP    || '0.0.0.0';


function dev(req,res,next){
    req.user={
            companyId:'abc',
            storeId:'cdf',
            name:'Eugene Li',
            role:['admin']
    }
    next()
}

app.use(express.static(__dirname+'/public'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(session({
    secret:'qvK4Frk2LV',
    resave: true,
    saveUninitialized: true,
    cookie:{

        maxAge: 3000000
    }
}));
app.use(dev)
app.use('/api/v1',api);








app.get('/',(req,res)=> {
    res.send('server is running at port : '+port)
})

    app.use(function(req,res,next)
{

    var err = new Error('Not Found');
    err.status = 404;

})
//hanlde server err 500
// app.use(function(err,req,res,next){
//
//     res.status(err.status || 500)
//         .send({message: err.message})
// });
app.listen(port,ip);
console.log('Server running at port' + port)
